package net.iterpalsTest;

import net.interpals.Pages.EditProfile;
import net.interpals.Pages.MyProfile;
import net.interpals.Pages.Search;
import net.interpals.Pages.Welcome;
import net.interpals.helpers.AbstractTest;
import net.interpals.listeners.ScreenshotListener;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import static net.interpals.utils.IConstant.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * @author Denis Kolovorotniy
 * @version 1.0
 */
@Listeners(ScreenshotListener.class)
public class FunctionalTest extends AbstractTest {


    @Test
    @Title("Авторизоваться как пользователь")
    @Description("Проверяет авторизацию пользователя")
    @Severity(value = SeverityLevel.CRITICAL)
    public void loginOnWebsite() {

        String expectedNickname = "Hi, BigHeadIgor!";

        String actualNickname = new Welcome(driver, wait)
                .openPage(BASE_URL)
                .login(LOGIN, PASSWORD)
                .checkUserNickname();

        assertEquals(actualNickname, expectedNickname);
    }


    @Test(priority = 1)
    @Title("Изменить статус пользователя")
    @Description("Проверяет функицию изменения статуса")
    @Severity(value = SeverityLevel.MINOR)
    public void shouldSetUpStatus() {

        String expectedStatus = "Hi everybody!";

        String actualStatus = new MyProfile(driver, wait)
                .openPage(BASE_URL + "/BigHeadIgor")
                .writeStatus(USER_STATUS)
                .checkTheStatus();

        assertEquals(actualStatus, expectedStatus);
    }


    @Test(priority = 1)
    @Title("Изменить фото пользователя")
    @Description("Проверяет функицию изменения фото пользователя")
    @Severity(value = SeverityLevel.NORMAL)
    public void shouldAddPhoto() {

        String actualPhoto = new MyProfile(driver, wait)
                .openPage(BASE_URL + "/BigHeadIgor")
                .uploadPhoto()
                .addAvatar(LINK_PHOTO)
                .checkAvatarAdded();

        assertTrue(actualPhoto.contains("1 Photo"));
    }

    @Test(priority = 1)
    @Title("Найти пользователя по нику")
    @Description("Проверяет функцию поиска пользователя по нику")
    @Severity(value = SeverityLevel.CRITICAL)
    public void shouldFindUserByNickname() {

        String expectedNickName = "Dendy322";

        String actualNickname = new Search(driver, wait)
                .openPage(BASE_URL + "/app/search")
                .writeNickname(expectedNickName)
                .goToUserPage()
                .checkUserNickname();

        assertTrue(actualNickname.contains(actualNickname));
    }

    @Test(priority = 1)
    @Title("Пишиет текст в разделе 'About'")
    @Description("Проверяет функцию добавления текста  в раздел 'About'")
    @Severity(value = SeverityLevel.NORMAL)
    public void shouldWriteAboutUser() {

        String textAboutMe = "Hello, My name is BigHeadIgor. I am here to makes friends.";

        String actualText = new EditProfile(driver, wait)
                .openPage(BASE_URL + "/app/profile/edit?section=about")
                .writeAboutText(textAboutMe)
                .saveChanges()
                .openPage(BASE_URL + "/BigHeadIgor")
                .checkTextInAboutField();

        assertTrue(actualText.contains(textAboutMe));
    }

    @Test(priority = 1)
    @Title("Пишиет текст в разделе 'Favorite Music'")
    @Description("Проверяет функцию добавления текста  в раздел 'Favorite Music'")
    @Severity(value = SeverityLevel.NORMAL)
    public void shouldWriteFavoriteMusic() {

        String textMusic = "I listen to any kind of music.";

        String actualText = new EditProfile(driver, wait)
                .openPage(BASE_URL + "/app/profile/edit?section=about")
                .writeFavoriteMusicText(textMusic)
                .saveChanges()
                .openPage(BASE_URL + "/BigHeadIgor")
                .checkTextInFavoriteMusicField();

        assertTrue(actualText.contains(textMusic));
    }

    @Test(priority = 1)
    @Title("Пишиет текст в разделе 'Favorite Movies'")
    @Description("Проверяет функцию добавления текста  в раздел 'Favorite Movies'")
    @Severity(value = SeverityLevel.NORMAL)
    public void shouldWriteFavoriteMovies() {

        String textMovie = "Sherlock Holms";

        String actualText = new EditProfile(driver, wait)
                .openPage(BASE_URL + "/app/profile/edit?section=about")
                .writeFavoriteMovieText(textMovie)
                .saveChanges()
                .openPage(BASE_URL + "/BigHeadIgor")
                .checkTextInFavoriteMoviesField();

        assertEquals(actualText, textMovie);
    }

    @Test(priority = 1)
    @Title("Пишиет текст в разделе 'Favorite Books'")
    @Description("Проверяет функцию добавления текста  в раздел 'Favorite books'")
    @Severity(value = SeverityLevel.NORMAL)
    public void shouldWriteFavoriteBooks() {

        String textBook = "Harry Potter";

        String actualText = new EditProfile(driver, wait)
                .openPage(BASE_URL + "/app/profile/edit?section=about")
                .writeFavoriteBookText(textBook)
                .saveChanges()
                .openPage(BASE_URL + "/BigHeadIgor")
                .checkTextInFavoriteBookField();

        assertTrue(actualText.contains(textBook));
    }

    @Test(priority = 1)
    @Title("Переходит на поисковую страницу из форума")
    @Description("Проверяет отображения страницы поиска потльзователя при переходе из страницы форума")
    public void shouldReturnLanguageExchange() {

        String expectedText = "Members Online";

        String actualText = new Welcome(driver, wait)
                .openPage("http://forum.interpals.net/")
                .getTextMembersOnline();

        assertEquals(actualText, expectedText);
    }

}
