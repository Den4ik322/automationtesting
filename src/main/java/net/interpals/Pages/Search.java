package net.interpals.Pages;

import net.interpals.helpers.AbstractPage;
import net.interpals.utils.IConstant;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * This class is imitate a page on a website. The page is accessed by the link {@link IConstant#BASE_URL}+ app/search.
 * The class describes the entire web elements and methods which are located on the page.
 * <p>
 * Extends  the class {@link AbstractPage}
 *
 * @author Denis Kolovorotniy
 * @version 1.0
 * @see AbstractPage
 */
public class Search extends AbstractPage {

    @FindBy(css = "#sCFields > div:nth-child(14) > span > input")
    @CacheLookup
    private WebElement usernameinput;

    @FindBy(css = "#sCSubmit > span > a")
    @CacheLookup
    private WebElement searchBtn;

    @FindBy(css = "#sRes > div.sResCont > div > div.sResMain > b > a")
    @CacheLookup
    private WebElement userPageBtn;


    public Search(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        PageFactory.initElements(driver, this);
    }

    @Override
    public AbstractPage click(WebElement element) {
        return null;
    }


    @Override
    @Step("Открывает страницу с поиском")
    public Search openPage(String url) {
        super.openPage(url);
        return this;
    }

    @Step("Пишет ник пользователя и наживает поиск")
    public Search writeNickname(String nickname) {

        waitElement("#sCFields > div:nth-child(14) > span > input");
        usernameinput.click();
        usernameinput.clear();
        usernameinput.sendKeys(nickname);

        waitElement("#sCSubmit > span > a");
        searchBtn.click();
        sleep(3000);

        return this;
    }

    @Step("Переходит на страницу найденого пользователя")
    public UserPage goToUserPage() {

        waitElement("#sRes > div.sResCont > div > div.sResMain > b > a");
        userPageBtn.click();
        sleep(3000);
        return new UserPage(driver, wait);
    }
}
