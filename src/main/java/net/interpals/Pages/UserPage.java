package net.interpals.Pages;

import net.interpals.helpers.AbstractPage;
import net.interpals.utils.IConstant;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * This class is imitate a page on a website. The page is accessed by the link {@link IConstant#BASE_URL}+ an user's nickname.
 * The class describes the entire web elements and methods which are located on the page.
 * <p>
 * Extends  the class {@link AbstractPage}
 *
 * @author Denis Kolovorotniy
 * @version 1.0
 * @see AbstractPage
 */
public class UserPage extends AbstractPage {

    @FindBy(css = "#profBodyTop > div.profileBox > h1")
    @CacheLookup
    private WebElement userNickname;

    public UserPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        PageFactory.initElements(driver, this);
    }

    @Override
    public AbstractPage click(WebElement element) {
        return null;
    }

    @Step("Проверяет ник полученого пользователя")
    public String checkUserNickname() {

        waitElement("#profBodyTop > div.profileBox > h1");

        return this.userNickname.getText();
    }
}
