package net.interpals.Pages;

import net.interpals.helpers.AbstractPage;
import net.interpals.utils.IConstant;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * This class is imitate a page on a website. The page is accessed by the link {@link IConstant#BASE_URL}+ /app/albums.
 * The class describes the entire web elements and methods which are located on the page.
 * <p>
 * Extends  the class {@link AbstractPage}
 *
 * @author Denis Kolovorotniy
 * @version 1.0
 * @see AbstractPage
 */
public class Photos extends AbstractPage {

    @FindBy(css = ".boxText")
    @CacheLookup
    private WebElement photoText;

    @FindBy(css = "#albUploadBox > input[type=\"submit\"]")
    @CacheLookup
    private WebElement saveAvatarBtn;

    @FindBy(css = "#newupload > a")
    @CacheLookup
    private WebElement simpleUploderBtn;

    @FindBy(css = "#fupFiles > input.fileUpload")
    @CacheLookup
    private WebElement chooseFileBtn;

    public Photos(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        PageFactory.initElements(driver, this);
    }

    @Override
    public AbstractPage click(WebElement element) {
        return null;
    }

    @Step("Загружает и сохраняет фото")
    public Photos addAvatar(String link) {

        waitElement("#newupload > a");
        simpleUploderBtn.click();
        chooseFileBtn.sendKeys(link);
        saveAvatarBtn.click();

        sleep(5000);
        return this;
    }

    @Override
    public Photos openPage(String url) {
        super.openPage(url);
        return this;
    }

    @Step("Проверяет что количество фото равно 1")
    public String checkAvatarAdded() {
        waitElement(".boxText");
        return photoText.getText();
    }


}
