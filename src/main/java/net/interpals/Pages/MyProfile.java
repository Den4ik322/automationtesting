package net.interpals.Pages;

import net.interpals.helpers.AbstractPage;
import net.interpals.utils.IConstant;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * This class is imitate a page on a website. The page is accessed by the link {@link IConstant#BASE_URL}+ /BigHeadIgor.
 * The class describes the entire web elements and methods which are located on the page.
 * <p>
 * Extends  the class {@link AbstractPage}
 *
 * @author Denis Kolovorotniy
 * @version 1.0
 * @see AbstractPage
 */
public class MyProfile extends AbstractPage {

    @FindBy(css = ".boxHead")
    @CacheLookup
    private WebElement editeTitle;
    //for status
    @FindBy(css = "#prStatQ")
    @CacheLookup
    private WebElement statusField;
    @FindBy(css = "#asMsg")
    @CacheLookup
    private WebElement statusText;
    @FindBy(css = "#asEditMsg")
    @CacheLookup
    private WebElement writeText;
    @FindBy(css = "#asEditSave")
    @CacheLookup
    private WebElement saveStatus;
    //------------------------------------------------------------
    @FindBy(css = "#profPhotos > a.profUplLink > div")
    @CacheLookup
    private WebElement uploadPhotoBtn;
    //
    @FindBy(css = ".profDataBox > h2:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
    @CacheLookup
    private WebElement editBtn;


    //------------------------------------------------------
    @FindBy(css = "#profBody > div.profDataBox > div:nth-child(2)")
    @CacheLookup
    private WebElement aboutMeText;
    @FindBy(css = "#profBody > div.profDataBox > div:nth-child(10)")
    @CacheLookup
    private WebElement favoriteMusicField;

    @FindBy(css = "#profBody > div.profDataBox > div:nth-child(12)")
    @CacheLookup
    private WebElement favoriteMoviesField;

    @FindBy(css = "#profBody > div.profDataBox > div:nth-child(16)")
    @CacheLookup
    private WebElement favoriteBooksField;

    public MyProfile(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        PageFactory.initElements(driver, this);
    }

    @Override
    @Step("Открывает веб-страницу с личным профилем")
    public MyProfile openPage(String url) {
        super.openPage(url);
        return this;
    }

    @Override
    public MyProfile click(WebElement element) {
        element.click();
        return this;
    }

    public EditProfile clickEditBtn() {
        click(editBtn);
        return new EditProfile(driver, wait);
    }

    @Step("Пишет статус и сохраняет его")
    public MyProfile writeStatus(String status) {
        click(statusField);

        waitElement("#asEditMsg");
        writeText.click();
        writeText.clear();
        writeText.sendKeys(status);

        saveStatus.click();

        return this;
    }


    @Step("Получает написанный статус")
    public String checkTheStatus() {
        waitElement("#asMsg");
        return statusText.getText();
    }


    @Step("Получает текст из раздела 'About' В профиле")
    public String checkTextInAboutField() {
        return aboutMeText.getText();
    }

    @Step("Получает текст из раздела 'Favorite Music' В профиле")
    public String checkTextInFavoriteMusicField() {
        return favoriteMusicField.getText();
    }

    @Step("Получает текст из раздела 'Favorite Movies' В профиле")
    public String checkTextInFavoriteMoviesField() {
        return favoriteMoviesField.getText();
    }

    @Step("Получает текст из раздела 'Favorite Books' В профиле")
    public String checkTextInFavoriteBookField() {
        return favoriteBooksField.getText();
    }

    @Step("переходит на страницу загрузки фото")
    public Photos uploadPhoto() {
        click(uploadPhotoBtn);

        return new Photos(driver, wait);
    }
}
