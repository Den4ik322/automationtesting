package net.interpals.Pages;

import net.interpals.helpers.AbstractPage;
import net.interpals.utils.IConstant;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * This class is imitate a page on a website. The page is accessed by the link {@link IConstant#BASE_URL}.
 * The class describes the entire web elements and methods which are located on the page.
 * <p>
 * Extends  the class {@link AbstractPage}
 *
 * @author Denis Kolovorotniy
 * @version 1.0
 * @see AbstractPage
 */
public class Welcome extends AbstractPage {


    @FindBy(css = "#topLoginEmail")
    @CacheLookup
    private WebElement loginFieldl;

    @FindBy(css = "#topLoginPassword")
    @CacheLookup
    private WebElement passwordFieldl;

    @FindBy(css = "#mainContent")
    @CacheLookup
    private WebElement mainContentBtn;

    @FindBy(css = "#topLogin > form:nth-child(1) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(3) > input:nth-child(1)")
    @CacheLookup
    private WebElement singInBnt;

    @FindBy(css = ".msg_error")
    @CacheLookup
    private WebElement wrongMessage;

    @FindBy(css = ".tdl")
    @CacheLookup
    private WebElement editBtn;

    @FindBy(css = "#bb4")
    @CacheLookup
    private WebElement forumSearchBtn;

    @FindBy(css = "#mainContainer > div > div.boxHead.allRnd > h1")
    @CacheLookup
    private WebElement membersOnlineText;


    @FindBy(css = "#mainContWBar > div > div.boxHead.topRnd > h1")
    @CacheLookup
    private WebElement languageExchangeText;


    public Welcome(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        PageFactory.initElements(driver, this);
    }

    @Override
    @Step("Открывает веб-страницу")
    public Welcome openPage(String url) {
        super.openPage(url);
        return this;
    }

    @Step("Ввести верные данные")
    public Home login(String login, String password) {
        loginFieldl.click();
        loginFieldl.clear();
        loginFieldl.sendKeys(login);

        passwordFieldl.click();
        passwordFieldl.clear();
        passwordFieldl.sendKeys(password, Keys.ENTER);
        return new Home(driver, wait);
    }

    @Override
    public MyProfile click(WebElement element) {
        editBtn.click();
        return new MyProfile(driver, wait);
    }

    @Step("Нажимает кнопку поиска и получает текст Members Online")
    public String getTextMembersOnline() {
        waitElement("#bb4");
        forumSearchBtn.click();
        waitElement("#mainContainer > div > div.boxHead.allRnd > h1");
        return membersOnlineText.getText();
    }

}
