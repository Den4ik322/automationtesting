package net.interpals.Pages;

import net.interpals.helpers.AbstractPage;
import net.interpals.utils.IConstant;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * <p>This class is imitate a page on a website. The page is accessed by the link {@link IConstant#BASE_URL}+ /app/account".
 * The class describes the entire web elements and methods which are located on the page.
 * <p>
 * Extends the class {@link AbstractPage}
 *
 * @author Denis Kolovorotniy
 * @version 1.0
 * @see AbstractPage
 */
public class Home extends AbstractPage {

    @FindBy(css = "#tdTitle")
    private WebElement welcomeUser;


    public Home(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        PageFactory.initElements(driver, this);
    }

    @Override
    public AbstractPage openPage(String url) {
        super.openPage(url);
        return this;
    }

    @Override
    public AbstractPage click(WebElement element) {
        return null;
    }

    @Step("Проверить ник авторизованного пользователя")
    public String checkUserNickname() {
        waitElement("#tdTitle");

        isLogin = true;
        return this.welcomeUser.getText();

    }

}
