package net.interpals.Pages;

import net.interpals.helpers.AbstractPage;
import net.interpals.utils.IConstant;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * This class is imitate a page on a website. The page is accessed by the link {@link IConstant#BASE_URL}+ app/profile/edit".
 * The class describes the entire web elements and methods which are located on the page.
 * <p>
 * Extends  the class {@link AbstractPage}
 *
 * @author Denis Kolovorotniy
 * @version 1.0
 * @see AbstractPage
 */
public class EditProfile extends AbstractPage {

    @FindBy(css = "#description")
    @CacheLookup
    private WebElement aboutTextField;

    @FindBy(css = "#music")
    @CacheLookup
    private WebElement musicTextField;

    @FindBy(css = "#movies")
    @CacheLookup
    private WebElement moviesTextField;

    @FindBy(css = "#books")
    @CacheLookup
    private WebElement booksTextField;

    @FindBy(css = ".mpTable > tbody:nth-child(1) > tr:nth-child(10) > td:nth-child(2) > input:nth-child(1)")
    @CacheLookup
    private WebElement saveBtn;

    public EditProfile(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        PageFactory.initElements(driver, this);
    }

    @Override
    public AbstractPage click(WebElement element) {
        element.click();
        return null;
    }

    @Override
    @Step("Открывает страницу настроек о пользователе")
    public EditProfile openPage(String url) {
        super.openPage(url);
        return this;
    }

    @Step("Пишет текс о себе")
    public EditProfile writeAboutText(String text) {
        waitElement("#description");
        aboutTextField.click();
        aboutTextField.clear();

        waitElement("#description");
        aboutTextField.sendKeys(text);
        return this;
    }

    @Step("Пишет текс о своей любимой музыки")
    public EditProfile writeFavoriteMusicText(String text) {
        waitElement("#music");
        musicTextField.click();
        musicTextField.clear();
        musicTextField.sendKeys(text);

        return this;
    }

    @Step("Сохраняет изменения")
    public MyProfile saveChanges() {

        waitElement(".mpTable > tbody:nth-child(1) > tr:nth-child(10) > td:nth-child(2) > input:nth-child(1)");
        this.saveBtn.click();
        return new MyProfile(driver, wait);
    }

    @Step("Пишет текс о своих любимых фильмах")
    public EditProfile writeFavoriteMovieText(String tvShow) {
        waitElement("#movies");
        moviesTextField.click();
        moviesTextField.clear();
        moviesTextField.sendKeys(tvShow);

        return this;
    }

    @Step("Пишет текс о своих любимых книгах")
    public EditProfile writeFavoriteBookText(String textBook) {

        waitElement("#books");

        booksTextField.click();
        booksTextField.clear();
        booksTextField.sendKeys(textBook);
        return this;
    }
}
