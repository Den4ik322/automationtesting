package net.interpals.helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Contains base settings for pages. Common methods and Webdriver.
 *
 * @author Denis Kolovorotniy
 * @version 1.0
 */
public abstract class AbstractPage {

    public boolean isLogin;
    protected WebDriver driver;
    protected WebDriverWait wait;

    public AbstractPage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
    }

    protected void sleep(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected void waitElement(String selector) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(selector)));
    }

    public AbstractPage openPage(String url) {
        driver.get(url);
        return this;
    }

    protected abstract AbstractPage click(WebElement element);

    public boolean isLogin() {
        return isLogin;
    }
}
