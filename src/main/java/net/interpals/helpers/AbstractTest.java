package net.interpals.helpers;

import jdk.nashorn.internal.ir.annotations.Ignore;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import ru.yandex.qatools.allure.annotations.Attachment;

import static net.interpals.utils.IConstant.CHROME_DRIVER;
import static net.interpals.utils.IConstant.CHROME_DRIVER_PATH;

/**
 * Contains base settings for test classes
 *
 * @author Denis Kolovorotniy
 * @version 1.0
 */
public abstract class AbstractTest {

    protected static WebDriver driver = null;
    protected static WebDriverWait wait = null;


    @Attachment(value = "Page screenshot {0}", type = "./Screenshot/png")
    public static byte[] makeScreenshot(String testName) {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }


    @BeforeTest(alwaysRun = true)
    public void setUp() {
        System.setProperty(CHROME_DRIVER, CHROME_DRIVER_PATH);

        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 5);
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();


    }

    @AfterTest(alwaysRun = true)
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}
