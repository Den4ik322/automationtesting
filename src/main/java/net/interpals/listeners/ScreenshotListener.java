package net.interpals.listeners;

import net.interpals.helpers.AbstractTest;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;
/**
 * @author Denis Kolovorotniy
 * @version 1.0
 */

public class ScreenshotListener extends TestListenerAdapter {

    @Override
    public void onTestFailure(ITestResult failtest) {
        if (ITestResult.FAILURE == failtest.getStatus()) {
            AbstractTest.makeScreenshot(failtest.getTestName());
        }
    }
}
