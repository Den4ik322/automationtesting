package net.interpals.utils;

/**
 * @author Denis Kolovorotniy
 * @version 1.0
 */
public interface IConstant {

    /**
     * Configuration cd for the FireFox browser
     * FIREFOX_DRIVER is the name of the FireFox
     * FIREFOX_DRIVER_PATH is the path where geckodriver is located.
     */
    String FIREFOX_DRIVER = "webdriver.gecko.driver";
    String FIREFOX_DRIVER_PATH = "src/main/resources/geckodriver.exe";

    /**
     * Configuration for the FireFox browser
     * CHROME_DRIVER is the name of the Google Chrome
     * CHROME_DRIVER_PATH is the path where chromedriver is located.
     */
    String CHROME_DRIVER = "webdriver.chrome.driver";
    String CHROME_DRIVER_PATH = "src/main/resources/chromedriver.exe";

    /**
     * Base url of a website.
     */
    String BASE_URL = "https://www.interpals.net";

    String USER_STATUS = "Hi everybody!";

    String LINK_PHOTO = "src/main/resources/avatar.jpg";

    /**
     * LOGIN is a user's login.
     * PASSWORD is a user's password.
     */
    String LOGIN = "BigHeadIgor";
    String PASSWORD = "haqygapuz64";
}
